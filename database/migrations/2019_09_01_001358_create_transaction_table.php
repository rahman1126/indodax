<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('sender_id');
            $table->bigInteger('receiver_id')->nullable();
            $table->string('transaction_type', 50)->default('transfer'); // transfer, payment
            $table->tinyInteger('transaction_status')->default(0); // 0 = pending, 1 = success, 2 = failed
            $table->decimal('money', 25, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
