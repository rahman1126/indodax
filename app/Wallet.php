<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'wallet';
    protected $fillable = ['user_id', 'money'];
    public $timestamps = false;

    public function findByUserId($user_id)
    {
        $wallet = $this->where('user_id', $user_id)->first();
        return $wallet;
    }

    // wallet belongs to user
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
