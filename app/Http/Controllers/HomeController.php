<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Validator;
use App\User;
use App\Wallet;
use App\Transaction;

class HomeController extends Controller
{
    protected $user;
    protected $wallet;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user, Wallet $wallet)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->wallet = $wallet;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        return view('home')
            ->with('user', $user);
    }

    /**
     * Transfer
     */
    public function transferView()
    {
        return view('home.transfer');
    }

    /**
     * Transfer History
     */
    public function transferHistory()
    {
        $records = Transaction::where('sender_id', Auth::id())
            ->orWhere('receiver_id', Auth::id())
            ->latest()
            ->paginate(10);

        return view('home.transfer-history')
            ->with('records', $records);
    }

    /**
     * Transfer Process
     * @method POST
     */
    public function transferProcess(Request $request)
    {

        $valid = Validator::make($request->all(), [
            'username'  => 'required|alpha_num|max:100',
            'nominal'   => 'required|numeric|min:1000'
        ]);

        if ($valid->fails()) {
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        } else {

            $sender = Auth::user();

            // check receiver by username
            $receiver = $this->user->findByUsername($request->username);
            if (!$receiver) {
                return redirect()->back()
                    ->with('error', 'User not found');
            }

            // check nominal with user money
            $sender_money = $sender->wallet->money;
            if ($sender_money < $request->nominal) {
                return redirect()->back()
                    ->with('error', 'Your money is not enough');
            }

            // process transfer
            try {

                DB::beginTransaction();

                $this->transfer($sender, $receiver, $request->nominal);
                $this->recordTransaction($sender->id, $receiver->id, 'transfer', 1, $request->nominal);

                DB::commit();

                return redirect(route('home'))
                    ->with('message', 'Transfer success');

            } catch (\Exception $e) {

                DB::rollBack();

                $this->recordTransaction($sender->id, $receiver->id, 'transfer', 2, $request->nominal);

                return redirect()->back()
                    ->with('error', 'Transfer fail');
            }

        }

    }

    // process transfer
    protected function transfer($sender, $receiver, $nominal)
    {

        // take money from sender
        $sender_wallet = $this->wallet->findByUserId($sender->id);
        $sender_wallet->money = $sender_wallet->money - $nominal;
        $sender_wallet->save();

        // add money to receiver
        $receiver_wallet = $this->wallet->findByUserId($receiver->id);
        $receiver_wallet->money = $receiver_wallet->money + $nominal;
        $receiver_wallet->save();

        return $sender_wallet;
    }

    // record transaction
    protected function recordTransaction($senderId, $receiverId, $transactionType, $transactionStatus, $nominal)
    {
        // record history
        $transaction = new Transaction;
        $transaction->sender_id = $senderId;
        $transaction->receiver_id = $receiverId;
        $transaction->transaction_type = $transactionType;
        $transaction->transaction_status = $transactionStatus;
        $transaction->money = $nominal;
        $transaction->save();

        return $transaction;
    }
}
