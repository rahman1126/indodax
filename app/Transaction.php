<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $fillable = ['user_id','receiver_id','transaction_type','transaction_status','money'];

    // sender
    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    // receiver
    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id');
    }
}
