<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('guest');

Auth::routes();
Route::post('/login', 'Auth\LoginController@authenticate')->name('login');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/transfer', 'HomeController@transferView')->name('transfer');
Route::post('/home/transfer', 'HomeController@transferProcess')->name('transfer-process');
Route::get('home/transfer-history', 'HomeController@transferHistory')->name('transfer-history');
