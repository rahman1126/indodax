@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('message'))
                <div class="alert alert-success mb-2" role="alert">
                    {{ session('message') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body text-center">
                    <h1>Hallo <b>{{ $user->username }}</b>,</h1>
                    <div class="money-box my-5">
                        <span>{{ number_format($user->wallet->money,0,",",".") }}</span>
                    </div>
                    <a href="{{ route('transfer') }}" class="btn btn-primary btn-lg">Transfer</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
