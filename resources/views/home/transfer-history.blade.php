@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Transfer History</div>
                <div class="card-body">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Date</th>
                          <th>Status</th>
                          <th>Nominal</th>
                          <th>Info</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($records as $item)
                        <tr>
                          <td>{{ $item->id }}</td>
                          <td>{{ $item->created_at->format('d F, Y H:i:s') }}</td>
                          <td>{!! ( $item->transaction_status == 1 ? '<i data-feather="check" color="green"></i>' : '<i data-feather="x" color="red"></i>' ) !!}</td>
                          <td>{{ number_format($item->money,0,",",".") }}</td>
                          <td>{!! ( $item->sender_id == Auth::id() ? '<i data-feather="corner-down-right" color="red"></i> Transfer to '. $item->receiver->username : '<i data-feather="corner-down-left" color="green"></i> Received from '. $item->sender->username ) !!}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $records->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
